const questions=[
    {
        question:"which is the largest animal on earth?",
        
          answers:[  {
              text:"shark", correct:false},
              {text:"bluewhale",correct:true},
              {text:"Lion", correct:false},
              {text:"Elephant",correct:false}

            
        ]
    },
    {
        question:"which is the smallest continent in the world ",
        
          answers:[  {
              text:"Asia", correct:false},
              {text:"Australia",correct:true},
              {text:"Artic", correct:false},
              {text:"Africa",correct:false}

            
        ]
    },
    {
        question:"what is the largest country in the world?",
        
          answers:[  {
              text:"Russia", correct:true},
              {text:"China",correct:false},
              {text:"America", correct:false},
              {text:"Italy",correct:false}

            
        ]
    },
    {
        question:"what is the longest river in India?",
        
          answers:[  {
              text:"Kaveri", correct:false},
              {text:"Ganga",correct:true},
              {text:"Brahmaputhra", correct:false},
              {text:"Sindhu",correct:false}

            
        ]
    }
];


const questionelement=document.getElementById("question");
const answerbtn=document.getElementById("answer-button");
const nextbutton=document.getElementById("next-btn");

let currentQuestionIndex=0;
let score=0;

function startquiz(){
    currentQuestionIndex=0;
    score=0;
    nextbutton.innerHTML="next";
    showquestion();
}
function showquestion(){
    resetState();
    let currentQuestion=questions[currentQuestionIndex];
    let questionNo=currentQuestionIndex + 1;
    questionelement.innerHTML=questionNo +"." + currentQuestion.question;

currentQuestion.answers.forEach(answer => {
    const button=document.createElement("button");
    button.innerHTML=answer.text;
    button.classList.add("btn");
    answerbtn.appendChild(button);
    if(answer.correct){
        button.dataset.correct=answer.correct;
    }
    button.addEventListener("click",selectAnswer)
});
}

function resetState(){
    nextbutton.style.display="none";
    while(answerbtn.firstChild){
        answerbtn.removeChild(answerbtn.firstChild);
    }
}

function selectAnswer(e){
    const selectbtn=e.target;
    const iscorrect =selectbtn.dataset.correct==="true";
    if(iscorrect){
        selectbtn.classList.add("correct");
        score++;
    }
    else{
         selectbtn.classList.add("incorrect");
    }
    Array.from(answerbtn.children).forEach(button=>{
        if(button.dataset.correct==="true"){
            button.classList.add("correct");
        }
        button.disabled=true;

    });
    nextbutton.style.display="block";
    

}

function showscore(){
    resetState();
    questionelement.innerHTML=`you are scored ${score}  out of ${questions.length}!`
    nextbutton.innerHTML="playagain";
    nextbutton.style.display="block"
}

function handlenextbutton(){
    currentQuestionIndex++;
    if(currentQuestionIndex<questions.length){
        showquestion();
    }else{
        showscore();
    }
}

nextbutton.addEventListener("click",()=>{
    if(currentQuestionIndex<questions.length){
        handlenextbutton();

    }else{
       startquiz(); 
    }
})

startquiz();
